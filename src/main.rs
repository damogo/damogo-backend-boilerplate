// teach me
#![deny(clippy::pedantic)]
// no unsafe
#![forbid(unsafe_code)]
// no unwrap
#![deny(clippy::result_unwrap_used)]
// no panic
#![deny(clippy::panic)]

mod context;
mod graphql;
mod juniper_hyper;

use crate::{
    context::Ctx,
    graphql::{mutations::Mutations, queries::Root},
};

use futures::FutureExt;
use header::HeaderValue;
use hyper::{header, Body, Request, Response, StatusCode};
use hyper::{
    service::{make_service_fn, service_fn},
    Method, Server,
};
use juniper::{EmptySubscription, RootNode};
use juniper_hyper::{graphiql, parse_req};
use log::info;
use log::{error, warn};
use std::{sync::Arc, time::Instant};
#[cfg(not(target_os = "windows"))]
use tokio::signal::{
    unix::{signal, Signal, SignalKind}
};

#[tokio::main]
async fn main() {
    pretty_env_logger::init_timed();
    let ctx = Ctx::init().await;
    match ctx {
        Ok(db) => {
            let root_node = Arc::new(RootNode::new(
                Root,
                Mutations,
                EmptySubscription::<Ctx>::new(),
            ));
            let ctx = db.clone();
            let new_service = make_service_fn(move |_| {
                let root_node = root_node.clone();
                let ctx = Arc::new(ctx.clone());
                async move {
                    Ok::<_, hyper::Error>(service_fn(move |req| {
                        let now = Instant::now();
                        let root_node = root_node.clone();
                        let ctx = ctx.clone();
                        async move {
                            match (req.method(), req.uri().path()) {
                                (&Method::GET, "/") => graphiql("/graphql", None).await,
                                (&Method::GET, "/graphql") | (&Method::POST, "/graphql") => {
                                    handle_graphql_request(req, root_node, &ctx, now).await
                                }
                                _ => {
                                    let mut response = Response::new(Body::empty());
                                    *response.status_mut() = StatusCode::NOT_FOUND;
                                    Ok(response)
                                }
                            }
                        }
                    }))
                }
            });
            let addr = ([0, 0, 0, 0], 3000).into();
            let server = Server::bind(&addr)
                .serve(new_service);
            info!("Listening on http://{}", addr);
            #[cfg(not(target_os = "windows"))]
            match signal(SignalKind::terminate()) {
                Ok(ref mut server_sigterm_listener) => {
                    let server_signal = sigterm_handler(server_sigterm_listener, "Server");
                    if let Err(e) = server.with_graceful_shutdown(server_signal).await {
                        error!("server error: {}", e);
                    }
                }
                Err(e) => {
                    error!("SIGTERM listener error: {}", e);
                    return ();
                },
            };
            #[cfg(target_os = "windows")]
            if let Err(e) = server.await {
                error!("server error: {}", e);
            }

        }
        Err(e) => error!("database error: {}", e),
    }
}

#[cfg(not(target_os = "windows"))]
async fn sigterm_handler(signal: &mut Signal, s: &str) {
    signal
        .recv()
        .map(|opt| {
            if opt.is_some() {
                info!("{} gracefully shutting down upon hearing SIGTERM", s);
            } else {
                error!("{} can no longer listen for SIGTERM", s);
            }
        })
        .await
}

async fn handle_graphql_request<'a>(
    req: Request<Body>,
    root_node: Arc<RootNode<'a, Root, Mutations, EmptySubscription<Ctx>>>,
    ctx: &Arc<Ctx>,
    now: Instant,
) -> hyper::Result<Response<Body>> {
    let response = match parse_req(req).await {
        Ok(req) => {
            let response = req.execute(&*root_node, ctx).await;
            let code = if response.is_ok() {
                StatusCode::OK
            } else {
                StatusCode::BAD_REQUEST
            };
            let mut resp = Response::new(Body::empty());
            *resp.status_mut() = code;
            resp.headers_mut().insert(
                header::CONTENT_TYPE,
                HeaderValue::from_static("application/json"),
            );
            if let Ok(body_string) = serde_json::to_string_pretty(&response) {
                let body = Body::from(body_string);
                *resp.body_mut() = body;
            } else {
                *resp.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
            }
            info!("Response time: {:?}", now.elapsed());
            resp
        }
        Err(response) => {
            warn!("Error parsing GraphQL query: {:#?}", response.body());
            response
        }
    };
    Ok(response)
}
