use crate::context::Ctx;
use juniper::graphql_object;

pub struct Mutations;

#[graphql_object(Context = Ctx)]
impl Mutations {
    fn dummy() -> bool {
        true
    }
}
