use sqlx::MySqlPool;
use std::fmt::Debug;

#[derive(Clone)]
pub struct Ctx {
    pool: MySqlPool,
}

impl Debug for Ctx {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("Context")
    }
}

impl Ctx {
    pub async fn init() -> sqlx::Result<Self> {
        Ok(Self {
            pool: MySqlPool::connect(env!("DATABASE_URL")).await?,
        })
    }
}
